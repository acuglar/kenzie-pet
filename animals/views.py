from django.shortcuts import render
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist

from .models import Animal, Characteristic, Group
from .serializers import AnimalSerializer


class AnimalView(APIView):
    def post(self, request):
        serializer = AnimalSerializer(data=request.data)
        # print('SERIALIZER: ', serializer)

        if not serializer.is_valid():
            
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        validated_data = serializer.validated_data
        # print('#VALIDATED_DATA: ', validated_data)

        # data = serializer.data
        # print('#DATA: ', data)

        group = validated_data.pop('group')
        characteristics = validated_data.pop('characteristics')

        #1:N
        group, created = Group.objects.get_or_create(**group)
        animal: Animal = Animal.objects.create(**validated_data, group=group)

        #N:N        
        for characteristic in characteristics:
            # characteristic -> OrderedDict([('key', 'value')])
            characteristic, created = Characteristic.objects.get_or_create(**characteristic)
            # characteristic -> value
            
            animal.characteristics.add(characteristic)
            
        serializer = AnimalSerializer(animal)

        return Response(serializer.data, status=status.HTTP_201_CREATED)
            

    def get(self, request, animal_id=''):
        print(APIView.allowed_methods)
        if animal_id:
            try: 
                animal = Animal.objects.get(id=animal_id)
                serializer = AnimalSerializer(animal)

                return Response(serializer.data, status=status.HTTP_200_OK)

            except ObjectDoesNotExist:
            
                return Response(status=status.HTTP_404_NOT_FOUND)
            
            # from django.shortcuts import get_object_or_404
            # animal = get_object_or_404(Animal, id=animal_id)
            # serializer = AnimalSerializer(animal)
            # return Response(serializer.data) 

        animals = Animal.objects.all()
        serializer = AnimalSerializer(animals, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


    def delete(self, request, animal_id):
        
        try: 
            animals = Animal.objects.get(id=animal_id)
            animals.delete()

            return Response(status=status.HTTP_204_NO_CONTENT)

        except ObjectDoesNotExist:
            
            return Response(status=status.HTTP_404_NOT_FOUND)

        # from django.shortcuts import get_object_or_404
        # animal = get_object_or_404(Animal, id=animal_id)
        # animal.delete()
        # return Response(status=status.HTTP_204_NO_CONTENT)

